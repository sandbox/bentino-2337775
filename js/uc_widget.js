(function($){
  $(document).ready(function(){
  
    $(document).on('keyup', '.field-widget-unit-conversion-widget .multiplicand-value', function(e){
      // console.log(e.target);
      var target = $(this).attr('id').split('-multiplicand-value')[0];
      
      var field_value = $(this).val().replace(/,/g, '');
      var rate = $('#' + target +' .rate').data('rate');
      var value = (field_value * rate).toFixed(2);
      
      value = value.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
      
      $('#'+ target +' .multiplier-value').val(value);
    });
    
    /*-------------------------------------------------\
    |                                                  |
    |           Numeric and Thousand seperator         |
    |                                                  |
    \-------------------------------------------------*/
    
    $('.field-widget-unit-conversion-widget .multiplicand-value').on({
      'click': selectAll,
      'keypress': onlyNumberAllowed,
      'keyup': thousandSeperator
    });
    
    /*--------------------------------------------------
    |                                                  |
    |           Field widget display script            |
    |                                                  |
    --------------------------------------------------*/

    if($('.uc-container').length) {

      if($('.uc-container').hasClass('uc-view-toggle')) {
        $('.uc-container.uc-view-toggle').prepend('<legend class="uc-toggle-button"><span>Rates</span></legend>');
        $('.uc-container.uc-view-toggle > .field-type-unit-conversion').hide();
        $(document).on('click', '.uc-toggle-button', function(){
          $(this).toggleClass('expanded');
          $(this).parent().find('.field-type-unit-conversion').slideToggle();
        });
      }
    }
    
    /*--------------------------------------------------
    |                                                  |
    |          Views supported display script          |
    |                                                  |
    --------------------------------------------------*/
    
    if($('.views-field').length) {
    
      // Field
      if($('.uc-group').hasClass('uc-group-view-default')) {
      
        // var uc_field = $('.uc-group').parent().parent().parent().parent();
        var uc_fields = [];
        $('.views-field').each(function(){
          if($(this).find('.uc-group').length) {
            uc_fields.push($(this));
          }
        });
        
        if(uc_fields.length) {
          for (i = 0; i < uc_fields.length; i++) {
            var uc_field = uc_fields[i];
            // Insert HTML
            uc_field.find('.field-content').append('<div class="uc-created"><span>Update on</span>' + $('.uc-created-date').first().val() +'</div>');
            
            // Wrap element
            uc_field.wrapInner('<div class="uc-wrapper"></div>');
          }
        }

      } else if($('.uc-group').hasClass('uc-group-view-toggle')) {
      
        // var uc_field = $('.uc-group').parent().parent().parent().parent();
        
        var uc_fields = [];
        $('.views-field').each(function(){
          if($(this).find('.uc-group').length) {
            uc_fields.push($(this));
          }
        });
        
        
        if(uc_fields.length) {
        
          for (i = 0; i < uc_fields.length; i++) {
          
            var uc_field = uc_fields[i];
            
            // Insert HTML
            uc_field.prepend('<legend class="uc-toggle-button"><span>Rates</span></legend>');
            uc_field.find('.field-content').append('<div class="uc-created"><span>Update on</span>' + $('.uc-created-date').first().val() +'</div>');
            
            // Wrap element
            uc_field.wrapInner('<div class="uc-wrapper"></div>');
          }
          
          $('.uc-toggle-button').parent().find('.field-content').hide();
            
          $(document).on('click', '.uc-toggle-button', function(){
            $(this).toggleClass('expanded');
            $(this).parent().find('.field-content').slideToggle();
          });
           
        }
        
        
      }
    }
    
  });
  
  var selectAll = function(e) {
    $(this).select();
  }
  
  var onlyNumberAllowed = function(e) {
    var code = e.keyCode || e.which;

    if(
      code != 8 && // Backspace key
      code != 9 && // Tab key
      code != 37 && // Left key
      code != 38 && // Up key
      code != 39 && // Right key
      code != 40 && // Down key
      (code < 48 || code > 57) // Inverse of numeric rage
    ) {
      if(e.metaKey || e.shiftKey || e.ctrlKey || e.altKey) { // Combination key check
        // console.log('ctrl');
      } else {
        e.preventDefault();
      }
    }
  }
  
  var thousandSeperator = function(e) {
    var code = e.keyCode || e.which;

    if((code >= 48 && code <= 57) || (code >= 96 && code <= 105) || (code == 8)) {
      var raw_value = $(this).val();
      var value = raw_value.split(',').join(''); 
      var comma = value.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
      $(this).val(comma);
    }
  }

})(jQuery);