(function($){
  $(document).ready(function(){
    $(document).on('keyup', '#unit-conversion-form .form-label input.form-text.required', function(e){
      $(this).parent().parent().parent().find('.form-key input').val($(this).val().toLowerCase().replace(' ', '_'));
    });
  });

})(jQuery);