<?php

function _field_unit_conversion_unit_conversion_form($form, &$form_state) {

  if(!isset($form_state['values'])) {
    $results = db_select('unit_conversion_table', 'uc_table')
                ->fields('uc_table')
                ->execute()
                ->fetchAll();
  }
  
  $form['#tree'] = TRUE;
  
  $form['unit_conversion_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Unit conversion'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#prefix' => '<div class="form-group unit-conversion-settings">',
    '#suffix' => '</div>'
  );
  
  $form['unit_conversion_settings']['field_rows'] = array(
    '#type' => 'container',
    '#attributes' => array(
      'id' => 'field-rows',
      'class' => array(
        'clearfix'
      ),
    ),
  );
  
  

  if(!isset($form_state['values'])) {
  
    $form_state['unit_conversion_rows'] = count($results);
    
    if($form_state['unit_conversion_rows']) {
      for($i = 0; $i < count($results); $i++) {
        $form['unit_conversion_settings']['field_rows'][$i] = _field_unit_conversion_unit_conversion_row($results[$i]);
      }
    } else {
      $form_state['unit_conversion_rows'] = 1;
      for($i = 0; $i < $form_state['unit_conversion_rows']; $i++) {
        $form['unit_conversion_settings']['field_rows'][$i] = _field_unit_conversion_unit_conversion_row();
      }
    }
    
    
  } else {
    if (empty($form_state['unit_conversion_rows'])) {
      $form_state['unit_conversion_rows'] = 1;
    }
    for ($i = 0; $i < $form_state['unit_conversion_rows']; $i++) {
      $form['unit_conversion_settings']['field_rows'][$i] = _field_unit_conversion_unit_conversion_row();
    }
  }

  
  $form['unit_conversion_settings']['add_row'] = array(
    '#type' => 'submit',
    '#value' => t('Add more'),
    '#submit' => array('_field_unit_conversion_add_one_row'),
    '#ajax' => array(
      'callback' => '_field_unit_conversion_add_one_row_callback',
      'wrapper' => array('field-rows')
    ),
  );
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Save configuration',
    '#submit' => array('field_unit_conversion_form_submit'),
  );
  
  return $form;
}

function _field_unit_conversion_add_one_row($form, &$form_state) {
  $form_state['unit_conversion_rows']++;
  $form_state['rebuild'] = TRUE;
}

function _field_unit_conversion_add_one_row_callback($form, $form_state) {
  return $form['unit_conversion_settings']['field_rows'];
}

function _field_unit_conversion_unit_conversion_row($default = null) {

  $row['row'] = array(
    '#type' => 'container',
    '#attributes' => array(
      'class' => array(
        'row',
        'clearfix'
      ),
    ),
  );

  $row['row']['multiplicand'] = array(
    '#type' => 'container',
    '#attributes' => array(
      'class' => array(
        'multiplicand-wrapper',
        'clearfix'
      ),
    ),
  );
  
  $row['row']['multiplicand']['title'] = array(
    '#markup' => '<p>'. t('Multiplicand') . '</p>',
  );
  
  $row['row']['multiplicand']['label_container'] = array(
    '#type' => 'container',
    '#attributes' => array(
      'class' => array(
        'form-group',
        'form-label'
      )
    )
  );
  $row['row']['multiplicand']['label_container']['label'] = array(
    '#type' => 'textfield',
    '#title' => t('Label'),
    '#default_value' => isset($default) ? $default->multiplicand_label : "",
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
    '#attributes' => array(
      'placeholder' => t('Unit label')
    ),
    '#suffix' => '<p class="help-block">Display label for readability. <br />Example: <em>US Dollar</em></p>'
  );
  $row['row']['multiplicand']['key_container'] = array(
    '#type' => 'container',
    '#attributes' => array(
      'class' => array(
        'form-group',
        'form-key'
      )
    )
  );
  $row['row']['multiplicand']['key_container']['key'] = array(
    '#type' => 'textfield',
    '#title' => t('Key'),
    '#default_value' => isset($default) ? $default->multiplicand_key : "",
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
    '#attributes' => array(
      'readonly' => 'readonly',
      'tabindex' => -1,
      'placeholder' => t('Unit key')
    ),
    '#suffix' => '<p class="help-block">Key value for machine. <br />Example: <em>us</em></p>'
  );
  $row['row']['multiplicand']['value_container'] = array(
    '#type' => 'container',
    '#attributes' => array(
      'class' => array(
        'form-group',
        'form-value'
      )
    )
  );
  $row['row']['multiplicand']['value_container']['value'] = array(
    '#type' => 'textfield',
    '#title' => t('Value'),
    '#default_value' => '1',
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
    '#attributes' => array(
      'readonly' => 'readonly',
      'tabindex' => -1,
      'placeholder' => t('Unit value'),
    ),
    '#suffix' => '<p class="help-block">Fixed to 1 for multiplication with multiplier value</p>'
  );
  
  $row['row']['multiplier'] = array(
    '#type' => 'container',
    '#attributes' => array(
      'class' => array(
        'multiplier-wrapper',
        'clearfix'
      ),
    ),
  );
  
  $row['row']['multiplier']['title'] = array(
    '#markup' => '<p>'.t('Multiplier').'</p>',
  );
  
  $row['row']['multiplier']['label_container'] = array(
    '#type' => 'container',
    '#attributes' => array(
      'class' => array(
        'form-group',
        'form-label'
      )
    )
  );
  $row['row']['multiplier']['label_container']['label'] = array(
    '#type' => 'textfield',
    '#title' => t('Label'),
    '#default_value' => isset($default) ? $default->multiplier_label : "",
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
    '#attributes' => array(
      'placeholder' => t('Unit label')
    ),
    '#suffix' => '<p class="help-block">Display label for readability. <br />Example: <em>Thai baht</em></p>'
  );
  $row['row']['multiplier']['key_container'] = array(
    '#type' => 'container',
    '#attributes' => array(
      'class' => array(
        'form-group',
        'form-key'
      )
    )
  );
  $row['row']['multiplier']['key_container']['key'] = array(
    '#type' => 'textfield',
    '#title' => t('Key'),
    '#default_value' => isset($default) ? $default->multiplier_key : "",
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
    '#attributes' => array(
      'readonly' => 'readonly',
      'tabindex' => -1,
      'placeholder' => t('Unit key')
    ),
    '#suffix' => '<p class="help-block">Key value for machine. <br />Example: <em>th</em></p>'
  );
  $row['row']['multiplier']['value_container'] = array(
    '#type' => 'container',
    '#attributes' => array(
      'class' => array(
        'form-group',
        'form-value'
      )
    )
  );
  $row['row']['multiplier']['value_container']['value'] = array(
    '#type' => 'textfield',
    '#title' => t('Value'),
    '#default_value' => isset($default) ? $default->multiplier_value : "",
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
    '#element_validate' => array('element_validate_number'),
    '#attributes' => array(
      'placeholder' => t('Unit value')
    ),
    '#suffix' => '<p class="help-block">Multiplication value <br />Example: <em>31</em></p>'
  );
  
  return $row;
}