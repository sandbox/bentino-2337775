<table class="uc-table">
  <thead>
    <tr>
      <th width="25%">Label</th>

      <th width="25%">Value</th>

      <th width="25%">Label</th>

      <th width="25%">Value</th>
    </tr>
  </thead>

  <tbody>
  
    <tr>
      <td class="legend" colspan="2">Multiplicand</td>
      <td class="legend" colspan="2">Multiplier</td>
    </tr>
  
    <?php foreach($results as $result): ?>
    <tr>
      <td><?php echo $result->multiplicand_label; ?></td>

      <td><?php echo $result->multiplicand_value; ?></td>

      <td><?php echo $result->multiplier_label; ?></td>

      <td><?php echo $result->multiplier_value; ?></td>
    </tr>
    <?php endforeach; ?>
    
  </tbody>
</table>
<div class="uc-latest-update"><span>Updated on: </span><?php echo date('d M Y',$results[0]->created); ?></div>