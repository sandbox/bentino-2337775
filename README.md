This module is a simple unit conversion module, it provided Unit Conversion field for auto conversion. Useful for conversion currency rate, length, temperature etc.

##Features
* Centralize configuration page
* Integrated with drupal's field system
* 2 display formatter (regular or toggle)
* Permission access for administration
* Views supported (Tested on Views 7.x-3.8)

###Requirements
* [jQuery update](https://www.drupal.org/project/jquery_update)

###Installation
1. Download this module
2. Extract to sites/all/modules
3. Enable module

###Instruction
1. Go to configuration page for setting up unit conversion
2. Add field type "Unit conversion" to any content type
3. Add content (That contain "Unit conversion" field)
4. Hooray! the unit will be auto conversion relative to the configuration in setting page

###Future work
* Separate to unit conversion group
* Conversion field can be delete not need value